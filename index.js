const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");

// Models
const Gig = require("./models/GIG");

const app = express();

// Middlewares
app.use(bodyParser.json());
app.use(cors());

// Connect to DB
const uri =
  "mongodb+srv://arjun:arjunisthebest@cluster0.w8nct.mongodb.net/zplus?retryWrites=true&w=majority";

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }, () =>
  console.log(`Connected To DataBase`)
);

app.get("/read", async (req, res) => {
  try {
    const gigs = await Gig.find();
    res.json(gigs);
  } catch (err) {
    res.json({ message: err });
  }
});

app.post("/add", async (req, res) => {
  const gig = new Gig({
    name: req.body.name,
    number: req.body.number,
    add: req.body.add,
    problem: req.body.problem,
    time: req.body.time,
  });
  try {
    const savedGig = await gig.save();
    res.json(savedGig);
  } catch (err) {
    res.json({ message: err });
  }
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server Running On Port ${PORT}`));
