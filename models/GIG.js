const mongoose = require("mongoose");

const GigSchema = mongoose.Schema({
  name: String,
  number: String,
  add: String,
  problem: String,
  time: String,
});

module.exports = mongoose.model("Gig", GigSchema);
